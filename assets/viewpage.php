<?php           

    include('connection.php');
    

    if(isset($_GET['task_id']))
    {
       $task_id =$_GET['task_id'];
    }


?>


<html> 
<?php
    include('Header.php');
?>
<style>
* {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Float four columns side by side */
.column {
  float: left;
  width: 25%;
  padding: 0 10px;
}

/* Remove extra left and right margins, due to padding */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive columns */
@media screen and (max-width: 600px) {
  .column {
    width: 100%;
    display: block;
    margin-bottom: 20px;
  }
}

/* Style the counter cards */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  padding: 16px;
  text-align: center;
  background-color: #ffffff;
}
</style>

<body>
        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row" style="">
                    <div class="col-lg-6">
                        <div class="card" style="margin-top: -61px;">
                            <div class="card-body">
                                <div id="pay-invoice">
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center"></h3>
                                        </div>
                                       
                                        <form action="" method="post" novalidate="novalidate">
                                        <tbody>
                        <?php
                     
            $sql ="SELECT  task_id as task_id, task_name,  descrip as descrip, cat.category as taskcat ,prio.priority  as task_priority ,log.u_name as asto, buss.business_priority ,cstat.current_status ,tsk.task_createdate,tsk.task_updatedate,tsk.task_duedate,tsk.task_remark,tsk.task_efforts
            FROM db_task.tbl_task tsk ,category cat,tbl_login log,tb_business buss,cur_status cstat,tb_priority prio
             where 
            tsk.task_cat = cat.cat_id
            AND tsk.assign_to = log.login_id 
            AND tsk.busi_priority = buss.business_id 
            AND tsk.curunt_status = cstat.status_id 
            AND tsk.task_priority= prio.pri_id
            AND tbl_task.task_id = '$task_id'";
            
          /*  "SELECT task_id, priority, tsk.*, cat.*,log.*, log.username
FROM
    tbl_task tsk
        INNER JOIN
    tb_priority prio ON tsk.task_priority = prio.pri_id 
    INNER JOIN category cat ON cat.cat_id = tsk.task_cat
    INNER JOIN tbl_login log ON log.login_id = tsk.assign_to
    INNER JOIN cur_status stat ON stat.status_id=tsk.curunt_status
    INNER JOIN tb_business buis ON buis.business_id=tsk.busi_priority
WHERE
    tsk.task_id = $task_id";*/
                           //echo $sql;
                           $query_run=mysqli_query($conn, $sql);
                           $user=mysqli_fetch_assoc($query_run);
                     
                           $result = $conn->query($sql);
                           if ($result->num_rows > 0) 
                           {
                                while($row = $result->fetch_assoc()) 
                           {
                            

                             ?>
                           
                           <div class="card text-center">
                              <div class="card-header">
                                    <h3>Task Records</h3>
                           </div>
                              <div class="card-body">
                                 <h5 class="card-title"></h5>
                                 <div class="row form-group ">
                                    <label for="task_id" style="font-size:22;font-color:#fffff0;" class="text-center">Task Id:<span style="color:#FF0000;text-align:right;font-size:24;"> <?php echo $row["task_id"];?></span></label>
                                 </div>
                                 <div class="row form-group">
                                 <label for="task_name" style="font-size:22;font-color:#fffff0;" class="text-center">Task Name:<span style="color:#FF0000;text-align:right;font-size:24;"> <?php echo $row["task_name"];?></span></label>
                                 </div>
                                 <div class="row form-group">
                                 <label for="descrip" style="font-size:22;font-color:#fffff0;" class="text-center">Description:<span style="color:#FF0000;text-align:right;font-size:24;"> <?php echo $row["descrip"];?></span></label>
                                 </div> 
                                 <div class="row form-group">
                                 <label for="asto" style="font-size:22;font-color:#fffff0;" class="text-center">Assign To:<span style="color:#FF0000;text-align:right;font-size:24;"> <?php echo $row["asto"];?></span></label>
                                 </div>    
                                 <div class="row form-group">
                                 <label for="taskcat" style="font-size:22;font-color:#fffff0;" class="text-center">Task Category:<span style="color:#FF0000;text-align:right;font-size:24;"> <?php echo $row["taskcat"];?></span></label>
                                 </div> 
                                 <div class="row form-group">
                                 <label for="taskprio" style="font-size:22;font-color:#fffff0;" class="text-center">Task Priority:<span style="color:#FF0000;text-align:right;font-size:24;"> <?php echo $row["task_priority"];?></span></label>
                                 </div> 
                                 <div class="row form-group">
                                 <label for="task_efforts" style="font-size:22;font-color:#fffff0;" class="text-center">Business Priority:<span style="color:#FF0000;text-align:right;font-size:24;"> <?php echo $row["business_priority"];?></span></label>
                                 </div> 
                                 <div class="row form-group">
                                 <label for="descrip" style="font-size:22;font-color:#fffff0;" class="text-center">Task Efforts:<span style="color:#FF0000;text-align:right;font-size:24;"> <?php echo $row["task_efforts"];?></span></label>
                                 </div> 
                                 <div class="row form-group">
                                 <label for="descrip" style="font-size:22;font-color:#fffff0;" class="text-center">Current Status:<span style="color:#FF0000;text-align:right;font-size:24;"> <?php echo ($row["current_status"]);?></span></label>
                                 </div> 
                                 <div class="row form-group">
                                 <label for="cdate" style="font-size:22;font-color:#fffff0;" class="text-center">Created Date:<span style="color:#FF0000;text-align:right;font-size:24;"> <?php echo $row["task_createdate"];?></span></label>
                                 </div> 
                                 <div class="row form-group">
                                 <label for="udate" style="font-size:22;font-color:#fffff0;" class="text-center">Updated Date:<span style="color:#FF0000;text-align:right;font-size:24;"> <?php echo $row["task_updatedate"];?></span></label>
                                 </div> 
                                 <div class="row form-group">
                                 <label for="ddate" style="font-size:22;font-color:#fffff0;" class="text-center">Due Date:<span style="color:#FF0000;text-align:right;font-size:24;"> <?php echo $row["task_duedate"];?></span></label>
                                 </div> 
                                 <div class="row form-group">
                                 <label for="remark" style="font-size:22;font-color:#fffff0;" class="text-center">Remark:<span style="color:#FF0000;text-align:right;font-size:24;"> <?php echo $row["task_remark"];?></span></label>
                                 </div> 
                              </div>
                              </div>
                        
                        <td>
                        <form action="" method="POST">
                       
                        </form>
                        </td>
                        </tr>
                        <?php
                           }
                           } 
                           else 
                           { 
                           echo "0 results"; 
                           }
                           ?>
                        </tbody>
                        
                        </div>
                        </div>
                     </div>
                                        </form>
                                   </div>
                                </div>

                            </div>
                        </div> 
                    </div>                              
                </div>
         </div>
    </div>
</html>                                   
<?php
    include('footer.php');
?>

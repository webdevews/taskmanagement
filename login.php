<?php 
    session_start();
    include('connection.php');
    $msg="";
    if(isset($_POST['login']))
     {
           $username = mysqli_real_escape_string($conn, $_POST['username']);
           $password = mysqli_real_escape_string($conn, $_POST['password']);
           $pass=md5($password);
           $sql ="SELECT u_role, username, upassword, u_name FROM db_task.tbl_login WHERE username='$username'";
           $query_run=mysqli_query($conn, $sql);
           $user=mysqli_fetch_assoc($query_run);
           if($user)
           {
                $pass_db=$user['upassword'];
                $role=$user['u_role'];
                $username=$user['u_name'];
                if($pass == $pass_db)
                {
                    $_SESSION['role']=$user['u_role']; 
                    $_SESSION['username']=$user['u_name']; 
                    if($role == 1)
                    {
                        header("location:admdashbord.php");
                    }
                    else
                    {
                        header("location:devdashbord.php");
                    }
                }
                else{
                    $error = "Password not matched";
                }
            }
            else
            {        
                $error = "Username not found";
                // '<span style=text-align:left;margin-left: -348px;">Username Not Found</span>';
            }      
        }
?>
<html>
    <head>
    <link rel="stylesheet" type="text/css" href="login.css">
   <head>Login Page </head>
<body>
  
      <div class="content">
          <header>Login</header>
          
            <form  action="login.php" method="post">
                <div class="field">
                    <span class="fa fa-user"></span>
                    <input type="text" name="username" value="<?php if(isset($username)){echo $username;} ?>" id="username" required placeholder="Email or Phone">    
                </div><br></br>
                <div class="field space">
                    <span class="fa fa-lock"></span>
                    <input type="password" name="password" id="password" required placeholder="Password">  
                    <span class="show">SHOW</span>
                </div>
               
                <span style="color:red;"><?php if(isset($error)){echo $error;} ?></span>
                <div>
                     <input type="submit" class="btn"name="login" value="login">
                </div>
               
            </form>
        </div>
    </div>
    <script  src="login.js"></script>
</body>
</html>
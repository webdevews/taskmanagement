<?php           
    include('connection.php');
?>
<style>
   /*dropdown css*/
   .drop{
   height: 31px;
   width: 239px;
   font-size: 15px;
   margin-bottom: 15px;
   margin-left: 22px;
   }
</style>
<html class="no-js" lang="en">
<?php
   include('Header.php');
?>
<head>
<title>Developer dashboard</title>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Devloper Dashbord</title>
      <link rel="stylesheet" href="vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
      <link rel="stylesheet" href="vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
   </head>
   </head>
   <body>
      <div class="content mt-3">
         <div class="animated fadeIn">
            <div class="row">
               <div class="col-lg-12">
                  <div class="card">
                     <div class="card-header">
                        <div class="row form-group">
                           <div class="col-md-4">
                           <label for="Assignto" name="asi">Assign To</label>
                              <select id="employee"  class="form-control"  onChange=myFunction()>
                                 <option value="" selected="selected" >Please select</option>
                                 <?php
                                    $sql = "SELECT task_id,task_name,cur.current_status,log.u_name,assign_to,task_efforts ,task_createdate,task_duedate, action from db_task.tbl_task task,db_task.tbl_login log, cur_status cur                                             where task.assign_to=log.login_id and cur.status_id=task.curunt_status";
                                       $resultset = mysqli_query($conn, $sql) or die("database error:". mysqli_error($conn));
                                       while( $rows = mysqli_fetch_assoc($resultset) ) { 
                                        ?>
                                 <option value="<?php 
                                    echo $rows["assign_to"]; ?>"><?php echo $rows["u_name"]; ?></option>
                                 <?php  }	?>
                              </select>
                           </div>
                           <div class="col-md-4">
                              <label for="Priority" name="prio">Priority</label>
                              <?php 
                                 $fetchEmployee = "select pri_id,priority from tb_priority";
                                 $employee = mysqli_query($conn, $fetchEmployee);
                                  ?>
                              <select name="prio" id="select" class="form-control">
                                 <option value="" selected disabled>Please select</option>
                                 <?php 
                                    while($rows = mysqli_fetch_assoc($employee)){ 
                                    echo '<option value="'.  $rows['pri_id'].  '">'. $rows['priority'] .'</option>' ;                                
                                     }
                                     ?>
                              </select>
                           </div>
                           <div class="col-md-4">
                              <label for="Priority" name="prio">Status</label>
                              <?php 
                                 $fetchEmployee = "select status_id,current_status from cur_status";
                                 $employee = mysqli_query($conn, $fetchEmployee);
                                  ?>
                              <select name="status" id="select" class="form-control">
                                 <option value="" selected disabled>Please select</option>
                                 <?php 
                                    while($rows = mysqli_fetch_assoc($employee)){ 
                                    echo '<option value="'.  $rows['status_id'].  '">'. $rows['current_status'] .'</option>' ;                                
                                     }
                                     ?>
                              </select>
                           </div>
                        </div>
                        <div class="card-body">
                           <table id="bootstrap-data-table-export" class="table table-striped table-bordered" >
                              <thead>
                                 <tr>
                                    <th style="background-color: #34495E;color: white;">Task ID</th>
                                    <th style="background-color: #34495E;color: white;">Task</th>
                                    <th style="background-color: #34495E;color: white;">Status</th>
                                    <th style="background-color: #34495E;color: white;">Effort</th>
                                    <th style="background-color: #34495E;color: white;">Assigned Date</th>
                                    <th style="background-color: #34495E;color: white;">Due Date</th>
                                    <th style="background-color: #34495E;color: white;">Action</th>
                                    <th style="background-color: #34495E;color: white;">View</th>
                                 </tr>
                              </thead>
                              <tbody>
                              <tbody class="task_tbl">
                                 <?php
                                    if(isset($_POST['value']))
                                    {
                                          $value= $_POST['value'];
                                          $updassign="SELECT task_id,task_name,curunt_status,task_efforts,task_createdate,task_duedate from db_task.tbl_task  where  assign_to='$value'";
                                          $resultset = mysqli_query($conn, $updassign) or die("database error:". mysqli_error($conn));
                                     }
                                    $result = $conn->query($sql);
                                    if ($result->num_rows> 0) 
                                     {
                                       //$i=0;
                                       while($row = $result->fetch_assoc()) 
                                       {
                                    
                                     ?>
                                 <tr>
                                    <form name="frm_grid" method="post" action="updatetask_developer.php">
                                       <td><?php echo $row["task_id"]; ?> <input type="hidden" name="grd_emp_id" value="<?php echo $row["task_id"]; ?>" > </td>
                                       <td><?php echo $row["task_name"]; ?>  </td>
                                       <td><?php echo $row["current_status"]; ?>  </td>
                                       <td><?php echo $row["task_efforts"]; ?>  </td>
                                       <td><?php echo $row["task_createdate"]; ?>  </td>
                                       <td><?php echo $row["task_duedate"]; ?>  </td>
                                    <form action="" method="post">
                                       <input type="hidden" name="task_id" value="<?php echo $row['task_id'];?>">
                                       <td><button type='submit'><i class='fa fa-pencil'></i></button> </td>
                                       <td><a href="viewpage.php?task_id=<?php echo $row['task_id']; ?>"><i class='  fa fa-eye'></i></a></td>
                                 </tr>
                                 </form>
                                 <?php
                                    }
                                        } 
                                        else 
                                        { 
                                        echo "0 results"; 
                                        }
                                    ?>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script>   
         function myFunction(){
           var value = document.getElementById('employee').value;
           $.ajax({
            url: "developer_dash_ajax.php",
            type: "POST",
            data:{value:value,assignTo:''},
            success: function(data){
            console.log(data); 
             $('.task_tbl').empty();
             $('.task_tbl').append(data);
              
            }
          });
         }
         
      </script>
      <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
      <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
      <script src="assets/js/init-scripts/data-table/datatables-init.js"></script>
   </body>
</html>

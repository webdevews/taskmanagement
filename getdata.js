$(document).ready(function(){  
	// code to get all records from table via select box
	$("#employee").change(function() {    
		var id = $(this).find(":selected").val();
		var dataString = 'task_id='+ id;    
		$.ajax({
			url: 'getemployee.php',
			dataType: "json",
			data: dataString,  
			cache: false,
			success: function(employeeData) {
			   if(employeeData) {
					$("#Task_id").text(employeeData.task_id);
					$("#Taks").text(employeeData.task_name);
					$("#Status").text(employeeData.cur_status);
					$("#records").show();		 
				} 	
			} 
		});
 	}) 
});